<?php

/*
 * Implementation of hook_provision_drupal_config().
 *
 * Super simple check for any additions that the profile wants to make to the
 * settings.php file.
 */
function provision_profile_settings_provision_drupal_config($uri, $data) {
  $lines = array();

  if (!empty(d()->profile)) {
    if (file_exists(d()->root . '/profiles/' . d()->profile . '/aegir.settings.php')) {
      $lines[] = '  // Additional site configuration settings from the install profile.';
      $lines[] = '  include_once(\'' . d()->root . '/profiles/' . d()->profile . '/aegir.settings.php' . '\');';
    }
  }

  return implode("\n", $lines);
}
